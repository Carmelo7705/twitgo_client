import React, {useState, useEffect} from 'react'
import { Button, Spinner } from 'react-bootstrap'
import { toast } from 'react-toastify'
import BasicLayout from '../../layouts/Basic/Basic'
import useAuth from '../../hooks/useAuth'
import BannerAvatar from '../../components/user/banneravatar/BannerAvatar'
import InfoUser from '../../components/user/infoUser/InfoUser'
import ListTweets from '../../components/ListTweets/ListTweets'
import { withRouter } from 'react-router-dom' //withRouter sirve para sacar los datos que tenemos en nuestra ruta actual
import { getUserApi } from '../../api/user'
import { getUserTweetsApi } from '../../api/tweets'


import './User.scss'

function User(props) {
  const { match, setRefreshCheckLogin } = props

  const [user, setUser] = useState(null)
  const [tweets, setTweets] = useState(null)
  const [page, setPage] = useState(1)
  const [loadingTweets, setLoadingTweets] = useState(false)

  useEffect(() => {
    getUserApi(match.params.id).then(response => {
      setUser(response)
      if(response == null) {
        toast.error("El usuario que has visitado no existe.")
      }
    }).catch(() => {
      toast.error("El usuario que has visitado no existe.")
    })
  }, [match.params]) //Siempre y cuando ocurra un cambio en params, ejecuta el useEffect(osea el getUserApi)

  useEffect(() => {
    getUserTweetsApi(match.params.id, 1).then(response => {
      setTweets(response)
    })
    .catch(() => {
      setTweets([])
    })
  }, [match.params])

  const moreData = () => {
    const pageTemp = page + 1;
    setLoadingTweets(true)

    getUserTweetsApi(match.params.id, pageTemp).then(response => {
      if(!response) {
        setLoadingTweets(0)
      } else {
        setTweets([...tweets, ...response]) //Vamos a agregar a los tweets que ya teníamos, en "tweets", los nuevos que estan en "response". todo esto,con spread operators.
        setPage(pageTemp)
        setLoadingTweets(false)
      }
    })
  }

  const currentUser = useAuth()

  return (
    <BasicLayout setRefreshCheckLogin={setRefreshCheckLogin} className="user">
      <div className="user__title">
        <h2>
          {user ? `${user.nombre} ${user.apellidos}` : 'Usuario no existe.'}
        </h2>
      </div>
      <div><BannerAvatar user={user} currentUser={currentUser} /></div>
      <InfoUser user={user} />
      <div className="user__tweets">
        <h3>Tweets</h3>
        {tweets && <ListTweets tweets={tweets} />}
        <Button onClick={moreData}>
          {!loadingTweets ? (
            loadingTweets !== 0 && 'Obtener mas tweets'
          ) : (
            <Spinner as="span" animation="grow" size="sm" role="status" aria-hidden="true" />
          )}
        </Button>
      </div>
    </BasicLayout>
  )
}

export default withRouter(User)
