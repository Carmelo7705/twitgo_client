import React from 'react'
import { Container, Row, Col } from 'react-bootstrap'
import LeftMenu from '../../components/LeftMenu/LeftMenu'

import './Basic.scss'

export default function Basic(props) {
  const { className, children, setRefreshCheckLogin } = props

  return (
    <Container className={`basic-layout ${className}`}>

      <Row>
        <Col xs="3" className="basic-layout__menu">
          <LeftMenu setRefreshCheckLogin={setRefreshCheckLogin} />
        </Col>
        <Col xs="9" className="basic-layout__content">
          {children}
        </Col>
      </Row>
    </Container>
  )
}
