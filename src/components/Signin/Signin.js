import React, { useState } from 'react'
import { Form, Button, Spinner } from 'react-bootstrap'
import { values, size } from 'lodash'
import { toast } from 'react-toastify'
import { isEmailValid } from '../../utils/isEmailValid'

import { signInApi, setTokenApi }  from '../../api/auth'

import "./Signin.scss"

export default function Signin(props) {
  const { setRefreshCheckLogin } = props
  const [formData, setFormData] = useState(initialFormValue())
  const [signinLoading, setSigninLoading] = useState(false)

  const onSubmit = e => {
    e.preventDefault();

    let validCount = 0;

    values(formData).some(value => {
      value && validCount++;
      return null;
    })

    if(size(formData) !== validCount) {
      toast.warning("Completa todos los campos del formulario.")
      return;
    }

    if(!isEmailValid(formData.email)) {
      toast.warning("El email es inválido.")
      return
    }
    
    setSigninLoading(true)
    signInApi(formData).then(response => {
      if(response.message){
        toast.warning(response.message)
        return
      } else{
        toast.success("Credenciales correctas, Ingresando...")
        setTokenApi(response.token)
        setRefreshCheckLogin(true)
        return
      }
    }).catch(() => {
      toast.error("Error del servidor")
    }).finally(() => {
      setSigninLoading(false)
    })
  }

  const onChange = e => {
    setFormData({...formData, [e.target.name]: e.target.value})
  }

  return (
    <div className="sign-in-form">
      <h2>Ingresa a Twitgo</h2>
      <Form onSubmit={onSubmit} onChange={onChange}>
        <Form.Group>
          <Form.Control type="email" name="email" defaultValue={formData.email} placeholder="Correo electrónico" />
        </Form.Group>
        <Form.Group>
          <Form.Control type="password" name="password" defaultValue={formData.password} placeholder="Password" />
        </Form.Group>
        <Button variant="primary" type="submit">
          { !signinLoading ? "Iniciar sesión" : <Spinner animation="border" /> }
        </Button>
      </Form>
    </div>
  )
}

function initialFormValue() {
  return {
    email: "",
    password: ""
  }
}
