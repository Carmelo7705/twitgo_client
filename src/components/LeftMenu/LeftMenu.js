import React, { useState } from 'react'
import { Button } from 'react-bootstrap'
import { Link } from 'react-router-dom' //Es el q nos sirve de <a></a>
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { toast } from 'react-toastify'
import { faHome, faUser, faUsers, faPowerOff } from '@fortawesome/free-solid-svg-icons'
import LogoWhite from "../../assets/png/logo_white.png"
import  TweetModal from '../Modal/TweetModal/TweetModal'
import { logoutApi } from '../../api/auth'

import useAuth from '../../hooks/useAuth'

import './LeftMenu.scss'

export default function LeftMenu(props) {
  const { setRefreshCheckLogin } = props
  const user = useAuth()

  const [showModal, setShowModal] = useState(false)

  const logout = () => {
    logoutApi()
    toast.warning("Te has desconectado.")
    setRefreshCheckLogin(true)
  }

  return (
    <div className="left-menu">
      <img src={LogoWhite} className="logo" alt="Twitgo" />

      <Link to="/">
        <FontAwesomeIcon icon={faHome} />
        Inicio
      </Link>
      <Link to="/users">
        <FontAwesomeIcon icon={faUsers} />
        Usuarios
      </Link>
      <Link to={`/user/${user?._id}`}>
        <FontAwesomeIcon icon={faUser} />
        Perfil
      </Link>
      <Link to="" onClick={logout}>
        <FontAwesomeIcon icon={faPowerOff} />
        Cerrar sesión
      </Link>

      <Button onClick={() => setShowModal(true)}>Twitear</Button>

      <TweetModal show={showModal} setShow={setShowModal} />
      
    </div>
  )
}
