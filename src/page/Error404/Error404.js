import React from 'react'
import { Link } from 'react-router-dom'
import img404 from '../../assets/png/error-404.png'
import logo from '../../assets/png/logo.png'

import "./Error404.scss"

export default function Error404() {
  return (
    <div className="error-404">
      <img src={logo} alt="Twitgo" />
      <img src={img404} alt="Error404" />
      <Link to="/">
        Volver al inicio
      </Link>
    </div>
  )
}
