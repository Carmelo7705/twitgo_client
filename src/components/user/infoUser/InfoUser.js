import React from 'react';
import moment from 'moment'
import localization from 'moment/locale/es'
import { Link, Calendar, Market } from '../../../utils/icons'

import './InfoUser.scss'

export default function InfoUser(props) {
  const { user } = props

  return (
    <div className="info-user">
      <h2 className="name">
        { user?.nombre } { user?.apellidos }
      </h2>
      <p className="email">
        { user?.email }
      </p>
      {user?.biografia && (
        <div className="description">
          {user.biografia}
        </div>
      )}
      <div className="more-info">
        {user?.ubicacion && (
          <p>
            <Market />
            {user.ubicacion}
          </p>
        )}
        {user?.sitio_web && (
          <a href={user.sitio_web} alt={user.sitio_web} target="_blank" rel="noopener noreferrer">
            <Link /> {user.sitio_web}
          </a>
        )}
        {user?.fecha_nacimiento && (
          <p>
            <Calendar />
            {moment(user.fecha_nacimiento).locale("es", localization).format('LL')}
          </p>
        )}
      </div>
    </div>
  )
}
