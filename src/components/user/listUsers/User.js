import React, { useState, useEffect } from 'react'
import { Media, Image } from 'react-bootstrap'
import { Link } from 'react-router-dom' 
import { API_HOST } from '../../../utils/constant'
import default_img from '../../../assets/png/default.png'
import { getUserApi } from '../../../api/user'

export default function User(props) {
  const { user } = props
  const [userInfo, setUserInfo] = useState(null)
  useEffect(() => {
    getUserApi(user.id).then(response => {
      setUserInfo(response)
    })
  }, [user])

  return (
    <Media as={Link} to={`user/${user.id}`} className="list-users__user">
      <Image width={60} height={60} roundedCircle className="mr-3" 
        src={userInfo?.avatar ? `${API_HOST}/get-avatar?id=${user.id}` : default_img} 
        alt={`${user.nombre} ${user.apellidos}`} />
      <Media.Body>
        <h5>
          {user.nombre} {user.apellidos}
        </h5>
        <p>{userInfo?.biografia}</p>
      </Media.Body>
    </Media>
  )
}
