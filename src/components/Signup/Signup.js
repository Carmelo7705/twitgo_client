import React, { useState } from 'react'
import { Row, Col, Form, Button, Spinner } from 'react-bootstrap'
import { values, size } from 'lodash'
import { toast } from 'react-toastify'

import { isEmailValid } from '../../utils/isEmailValid'

import { signUpApi } from '../../api/auth'

import './signup.scss'

export default function Signup(props) {
  const { setShowModal } = props
  const [formData, setFormData] = useState(initialFormValue())
  const [signupLoading, setSignupLoading] = useState(false)

  const onSubmit = (e) => {
    e.preventDefault();

    let validCount = 0;

    values(formData).some(value => {
      value && validCount++
      return null
    })
    
    if(validCount !== size(formData)) {
      toast.warning("Debes llenar todos los campos.")
      return
    }

    if(!isEmailValid(formData.email)) {
      toast.warning("Formato de email es inválido.")
      return
    }

    if(formData.password !== formData.repeatPassword) {
      toast.warning("Las contraseñas no coinciden.")
      return
    }

    if(size(formData.password) < 6) {
      toast.warning("La contraseña debe tener al menos 6 caracteres.")
      return
    }

    setSignupLoading(true)
    signUpApi(formData).then(response => {
      if(response.code) {
        toast.warning(response.message)
      } else {
        toast.success("Registro exitoso!")
        setShowModal(false)
        setFormData(initialFormValue)
      }
    }).catch(() => {
      toast.warning("Error en el servidor, intente mas tarde.")
    })
    .finally(() => {
      setSignupLoading(false)
    })
  }

  const onChange = (e) => { //Este onchange funciona en la equita form mientras los campos no contengan ni select, checkbox ni nada de eso, em casp de tenerlo. el onchange de la etiqueta form, pasaría a cada input por separado. 
    setFormData({...formData, [e.target.name]: e.target.value}) //...formData = SpreadOperator
  }

  return (
    <div className="sign-up-form">
      <h2>Crea tu cuenta</h2>
      <Form onSubmit={onSubmit} onChange={onChange}> 
        <Form.Group>
          <Row>
            <Col>
              <Form.Control type="text" placeholder="Nombre" name="nombre" defaultValue={formData.nombres} />
            </Col>
            <Col>
              <Form.Control type="text" placeholder="Apellidos" name="apellidos" defaultValue={formData.apellidos} />
            </Col>
          </Row>
        </Form.Group>
        <Form.Group>
          <Form.Control type="email" placeholder="Correo electronico" name="email" defaultValue={formData.email}  />
        </Form.Group>
        <Form.Group>
          <Row>
            <Col>
              <Form.Control type="password" placeholder="Password" name="password" defaultValue={formData.password} />
            </Col>
            <Col>
              <Form.Control type="password" placeholder="Repetir contraseña" name="repeatPassword" defaultValue={formData.repeatPassword}  />
            </Col>
          </Row>
        </Form.Group>
        <Button variant="primary" type="submit">
          {!signupLoading ? "Registrarse" : <Spinner animation="border" />}
        </Button>
      </Form>
    </div>
  )
}

function initialFormValue() {
  return {
    nombre: "",
    apellidos: "",
    email: "",
    password: "",
    repeatPassword: ""
  };
}