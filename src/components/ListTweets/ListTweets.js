import React, { useState,useEffect } from 'react'
import { Image } from 'react-bootstrap'
import { map } from 'lodash'
import moment from 'moment'
import user_default from '../../assets/png/default.png'
import { API_HOST } from '../../utils/constant'
import { replacesURLWithLinks } from '../../utils/function'
import { getUserApi } from '../../api/user'

import "./ListTweets.scss"

export default function ListTweets(props) {
  const { tweets } = props

  return (
    <div className="list-tweets">
      {map(tweets, (tweet, index) => (
        <Tweet key={index} tweet={tweet} />
      ))}
    </div>
  )
}

function Tweet(props) {
  const { tweet } = props;
  const [userInfo, setUserInfo] = useState(null);
  const [avatarUrl, setAvatarUrl] = useState(null);

  useEffect(() => {
    getUserApi(tweet.user_id).then((response) => {
      setUserInfo(response);
      setAvatarUrl(
        response?.avatar
          ? `${API_HOST}/get-avatar?id=${response.id}`
          : user_default
      );
    });
  }, [tweet]);
  
  return (
    <div className="tweet">
      <Image className="avatar_msg" src={avatarUrl} roundedCircle />
      <div>
        <div className="name">
          {userInfo?.nombre} {userInfo?.apellidos}
          <span>{moment(tweet.fecha).calendar()}</span>
        </div>
        <div dangerouslySetInnerHTML={{__html:replacesURLWithLinks(tweet.message)}} />
      </div>
    </div>
  );
}