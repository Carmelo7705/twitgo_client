import { API_HOST, TOKEN } from '../utils/constant'
import jwtDecode from 'jwt-decode'

export function signUpApi(user) {
  const url = `${API_HOST}/register`

  const userTemp = {
    ...user, //Ponme todos los datos de usuario
    email: user.email.toLowerCase(), //pero en la propiedad de email, siempre en minusculas
    fecha_nacimiento: new Date() //y mete la fecha actual en fecha_nacimiento
  }

  delete userTemp.repeatPassword
  const params = {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(userTemp)
  };

  return fetch(url, params).then(response => {
    if(response.status >= 200 && response.status < 300) {
      return response.json()
    }

    return { code: 404, message: "Email no disponible." }

  }).then(result => {
    return result;
  }).catch(err => {
    return err;
  })
}

export function signInApi(user) {
  const url = `${API_HOST}/login`

  const userTemp = {
    ...user,
    email: user.email.toLowerCase()
  }

  const params = {
    method: "POST",
    headers: {
      "Content-Type": "application/json"
    },
    body: JSON.stringify(userTemp)
  };

  return fetch(url, params).then(response => {
    if(response.status >= 200 && response.status < 300) {
      return response.json()
    }

    return { message: "Credenciales incorrectas." }

  }).then(result => {
    return result;
  }).catch(err => {
    return err;
  })
}

export function setTokenApi(token) {
  localStorage.setItem(TOKEN, token)
}

export function isExpired(token) {
  const { exp } = jwtDecode(token)
  const expire = exp * 1000;
  const timeout = expire - Date.now()

  if(timeout < 0) {
    return true
  }

  return false
}

export function logoutApi() {
  return localStorage.removeItem(TOKEN)
}

export function getTokenApi() {
  return localStorage.getItem(TOKEN)
}

export function isUserLoggedApi() {
  const token = getTokenApi()

  if(!token) {
    logoutApi()
    return null
  }

  if(isExpired(token)) {
    logoutApi()
  }

  return jwtDecode(token)
}