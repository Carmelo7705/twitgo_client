import { API_HOST } from '../utils/constant'
import { getTokenApi } from './auth'

export function checkFollowApi(userId) {
  const url = `${API_HOST}/consulta-relacion?id=${userId}`

  const params = {
    headers: {
      'Authorization': `Bearer ${getTokenApi()}`
    }
  }

  return fetch(url, params).then(response => {
    return response.json()
  }).then(result => {
    return result;
  }).catch(err => {
    return err;
  })
}

export function followUserApi(userId) {
  const url = `${API_HOST}/alta-relacion?id=${userId}`

  const params = {
    method: "GET",
    headers: {
      'Authorization': `Bearer ${getTokenApi()}`
    }
  }

  return fetch(url, params).then(response => {
    return response.json()
  }).then(result => {
    return result;
  }).catch(err => {
    return err;
  });
}

export function unFollowUserApi(userId) {
  const url = `${API_HOST}/baja-relacion?id=${userId}`

  const params = {
    method: "DELETE",
    headers: {
      'Authorization': `Bearer ${getTokenApi()}`
    }
  }

  return fetch(url, params).then(response => {
    return response.json()
  }).then(result => {
    return result;
  }).catch(err => {
    return err;
  });
}

export function getFollowsApi(paramsUrl) {
  const url = `${API_HOST}/users-list?${paramsUrl }`

  const params = {
    headers: {
      'Authorization': `Bearer ${getTokenApi()}`,
    }
  }

  return fetch(url, params).then(response => {
    return response.json();
  }).then(result => {
    return result;
  }).catch(err => {
    return err;
  })
}