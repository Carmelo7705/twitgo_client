import React, { useState ,useEffect } from 'react'
import { Button } from 'react-bootstrap'  
import ConfigProfileModal from '../../Modal/configProfile/ConfigProfile'
import EditUserForm from '../../user/formEdit/FormEdit'
import user_default from '../../../assets/png/default.png'
import { API_HOST } from '../../../utils/constant'

import { checkFollowApi, followUserApi, unFollowUserApi } from '../../../api/follow'

import "./BannerAvatar.scss"

export default function BannerAvatar(props) {
  const { user, currentUser } = props
  const [showModal, setShowModal] = useState(false)
  const [follow, setFollow] = useState(null)
  const [reloadFollow, setReloadFollow] = useState(false)

  const urlBanner = user?.banner ? `${API_HOST}/get-banner?id=${user.id}` : null
  const urlAvatar = user?.avatar ? `${API_HOST}/get-avatar?id=${user.id}` : user_default

  useEffect(() => {
    checkFollowApi(user?.id).then(response => {
      if(user) {
        if(response?.status) {
          setFollow(true)
        } else {
          setFollow(false)
        }
      }
      setReloadFollow(false)
    })

  }, [user,reloadFollow])

  const onFollow = () => {
    followUserApi(user.id).then(response => {
      setReloadFollow(true)
    }).catch(err => {
      console.log(err)
    })
  }

  const onOnFollow = () => {
    unFollowUserApi(user.id).then(response => {
      setReloadFollow(true)
    })
  }

  return (
    <div className="banner-avatar" style={{ backgroundImage: `url('${urlBanner}')` }}>
      <div className="avatar" style={{ backgroundImage: `url('${urlAvatar}')` }}></div>
      {user && (
        <div className="options">
          { currentUser._id === user.id && (
            <Button onClick={() => setShowModal(true)}>Editar perfil</Button>
          )}
          {currentUser._id !== user.id && (
            follow !== null && (
              (follow ? <Button className="unfollow" onClick={onOnFollow}><span>Siguiendo</span></Button> 
                : <Button onClick={onFollow}>Seguir</Button>) 
            )
          )}

          <ConfigProfileModal show={showModal} setShow={setShowModal} title="Editar mi perfil"> 
            <EditUserForm user={user} setShow={setShowModal} />
          </ConfigProfileModal>
        </div>
      )}
    </div>
  )
}

/** user && significa: Si el user existe, !!user si no existe. */
