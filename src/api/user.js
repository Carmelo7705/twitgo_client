import { API_HOST } from '../utils/constant'
import { getTokenApi } from './auth'

export function getUserApi(id) {
  const url = `${API_HOST}/my-profile?id=${id}`

  const params = {
    method: "GET",
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${getTokenApi()}`
    }
  }

  return fetch(url, params).then(response => {
    if(response.status >= 400) throw null //En caso de que el status sea igual a error, throw null salta de una al catch
    return response.json()
  }).then(result => {
    return result;
  }).catch(err => {
    return err;
  })
}

export function uploadBannerApi(file) {
  const url = `${API_HOST}/upload-banner`

  const formData = new FormData();
  formData.append('banner', file)

  const params = {
    method: "POST",
    headers: {
      'Authorization': `Bearer ${getTokenApi()}`
    },
    body: formData
  }

  return fetch(url, params)
  .then(response => {
    return response.json();
  }).then(result => {
    return result;
  }).catch(err => {
    return err;
  });

}

export function uploadAvatarApi(file) {
  const url = `${API_HOST}/upload-avatar`

  const formdata = new FormData();
  formdata.append('avatar', file)

  const params = {
    method: "POST",
    headers: {
      'Authorization': `Bearer ${getTokenApi()}`
    },
    body: formdata
  }

  return fetch(url, params).then(response => {
    return response.json()
  }).then(result => {
    return result;
  }).catch(err => {
    return err;
  })

}

export function updateUserApi(data) {
  const url = `${API_HOST}/edit-profile`

  const params = {
    method: "PUT",
    headers: {
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${getTokenApi()}`
    },
    body: JSON.stringify(data)
  };

  return fetch(url, params).then(response => {
    return response
  }).catch(err => {
    return err;
  })
}