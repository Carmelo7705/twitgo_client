import React, { useState, useEffect } from 'react'
import { Button, Spinner } from 'react-bootstrap'

import BasicLayout from '../../layouts/Basic/Basic'
import ListTweets from '../../components/ListTweets/ListTweets'
import { getTweetsFollowersApi } from '../../api/tweets'
import "./Home.scss"

export default function Home(props) {
  const { setRefreshCheckLogin } = props

  const [tweets, setTweets] = useState(null)
  const [page, setPage] = useState(1)
  const [loading, setLoading] = useState(false)

  useEffect(() => {
    getTweetsFollowersApi(page).then(response => {
      if(!tweets && response) {
        setTweets(formatModel(response))
      } else {
        if(!response) {
          setLoading(0)
        } else {
          const data = formatModel(response)
          setTweets([...tweets, ...data])
          setLoading(false)
        }
      }
      
    })
    .catch(() => {});
  }, [page])

  const moreData = () => {
    setLoading(true)
    setPage(parseInt(page) + 1)

  }

  return (
    <BasicLayout setRefreshCheckLogin={setRefreshCheckLogin} className="home">
      <div className="home__title">
        <h2>Inicio</h2>
      </div>
      { tweets && <ListTweets tweets={tweets} /> }
      <Button className="load-more" onClick={moreData}>
        { !loading ? (
          loading !== 0 ? 'Cargas más tweets...' : "No hay mas tweets"
        ) :
          <Spinner as="span" animation="grow" size="sm" role="status" aria-hidden="true" /> }
      </Button>
    </BasicLayout>
  )
}

function formatModel(tweets) {
  const tempTweets = [];

  tweets.forEach(element => {
    tempTweets.push({
      _id: element._id,
      user_id: element.user_relation_id,
      message: element.Tweet.message,
      fecha: element.Tweet.fecha
    })
  })

  return tempTweets;
}
