import React, { useState, useEffect } from 'react'
//Components project
import BasicLayout from '../../layouts/Basic'
import { getFollowsApi } from '../../api/follow'
import ListUsers from '../../components/user/listUsers'
import "./Users.scss"
//Components installed
import { Spinner, ButtonGroup, Button } from 'react-bootstrap'
import { withRouter } from 'react-router-dom'
import queryString from 'query-string'
import { isEmpty } from 'lodash'
import { useDebouncedCallback } from 'use-debounce'

function Users(props) {
  const { setRefreshCheckLogin, location, history, match } = props

  const [users, setUsers] = useState(null)
  const [typeUser, setTypeUser] = useState(match.params.type || "follow") //Si en mi ruta, en los params, tengo un valor en type, coloca ese, sino, pon "follow"
  const params = useUsersQuery(location)
  const [btnLoading, setBtnLoading] = useState(false)

  useEffect(() => {
    getFollowsApi(queryString.stringify(params)).then(response => {
      if(params.page == 1) {
        if(isEmpty(response)) {
          setUsers([])
        } else {
          setUsers(response)
        }

      } else {
        if(!response) {
          setBtnLoading(0)
        } else {
          setUsers(...users, ...response)
          setBtnLoading(false)
        }
      }
    })
    .catch(() => {
      setUsers([]);
    })

    
  }, [location])

  const onChangeType = type => {
    setUsers(null);
    if(type === "new") {
      setTypeUser("new")
    } else {
      setTypeUser("follow")
    }

    history.push({
      search: queryString.stringify({type: type, page: 1, search: ""})
    })
  }
  
  const [onSearch] = useDebouncedCallback((val) => {
    setUsers(null)
    history.push({
      search: queryString.stringify({...params, search: val, page: 1})
    })

  }, 200); //Es decir, 0,2 segundos

  const moreData = () => {
    setBtnLoading(true)
    const newPage = parseInt(params.page) + 1;
    history.push({
      search: queryString.stringify({
        ...params,
        page: newPage
      })
    });
  }

  return (
    <BasicLayout className="users" setRefreshCheckLogin={setRefreshCheckLogin}>
      <div className="users__title">
        <h2>Usuarios</h2>
        <p>
          <input type="text" placeholder="Busca un usuario..." 
            onChange={ (event) => onSearch(event.target.value)} />
        </p>
      </div>

      <ButtonGroup className="users__options">
        <Button className={typeUser === "follow" && "active"} onClick={() => onChangeType("follow")}>
          Siguiendo
        </Button>
        <Button className={typeUser === "new" && "active"} onClick={() => onChangeType("new")}>
          Nuevos
        </Button>
      </ButtonGroup>

      { !users ? (
        <div className="users__loading">
          <Spinner animation="border" variant="info" />
          Buscando usuarios...
        </div>
      ) : (
        <>
          <ListUsers users={users} />
          <Button className="load-more" onClick={moreData}>
            { !btnLoading ? (
              btnLoading !== 0 && "Cargar mas usuarios"
            ) : (
              <Spinner as="span" animation="grow" size="sm" role="status" aria-hidden="true" />
            )}
          </Button>
        </>
      ) }
    </BasicLayout>
  )
}

function useUsersQuery(location) {
  const { page = 1, type = "follow", search = ""  } = queryString.parse(location.search)
  /** Busca en la ruta page, type y search, si no existen, los valores colocados actuales, son
   * los valores por defectos */

  return { page, type, search };
}

export default withRouter(Users)
