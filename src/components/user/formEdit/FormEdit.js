import React, { useState, useCallback} from 'react'
import { Form, Row, Col, Button, Spinner } from 'react-bootstrap'
import DatePicker from 'react-datepicker'
import { useDropzone } from 'react-dropzone'
import { toast } from 'react-toastify'
import es from 'date-fns/locale/es'
import { API_HOST } from '../../../utils/constant'
import { Camera } from '../../../utils/icons'

import { uploadBannerApi, uploadAvatarApi, updateUserApi } from '../../../api/user'

import "./FormEdit.scss"

export default function FormEdit(props) {
  const { user, setShow } = props;
  const [formData, setFormData] = useState(initialFormValue(user)) //Se guarda user en formData, debido a que nunca se debe manejar o editar la data directamente.
  const [bannerUrl, setBannerUrl] = useState(
    user?.banner ? `${API_HOST}/get-banner?id=${user.id}` : null
  )
  const [avatarUrl, setAvatarUrl] = useState(
    user?.avatar ? `${API_HOST}/get-avatar?id=${user.id}` : null
  )

  const [bannerFile, setBannerFile] = useState(null)
  const [avatarFile, setAvatarFile] = useState(null)
  const [loading, setLoading] = useState(false)

  const onDropBanner = useCallback((acceptedFile) => {
    const file = acceptedFile[0]
    setBannerUrl(URL.createObjectURL(file))
    setBannerFile(file)
  })

  const { getRootProps: getRootBannerProps, getInputProps: getInputBannerProps } = useDropzone({
    accept: "image/jpeg, image/png",
    noKeyboard: true,
    multiple: false,
    onDrop: onDropBanner
  })

  const onDropAvatar = useCallback((acceptedFile) => {
    const file = acceptedFile[0]
    setAvatarUrl(URL.createObjectURL(file))
    setAvatarFile(file)
  })

  const { getRootProps: getRootAvatarProps, getInputProps: getInputAvatarProps } = useDropzone({
    accept: "image/jpeg, image/png",
    noKeyboard: true,
    multiple: false,
    onDrop: onDropAvatar
  })

  const onChange = (e) => {
    setFormData({...formData, [e.target.name]: e.target.value})
  }

  const onSubmit = async (e) => {
    e.preventDefault();
    setLoading(true)
    //console.log(formData)

    if(bannerFile) { //Si bannerFile existe, el usuario ha subido nuevo banner.
      await uploadBannerApi(bannerFile).catch(() => {
        toast.error("Error al subir el banner.")
      })
    }

    if(avatarFile) { //Si avatarFile existe, el usuario ha subido nuevo banner.
      await uploadAvatarApi(avatarFile).catch(() => {
        toast.error("Error al subir el avatar.")
      })
    }

    await updateUserApi(formData).then(() => {
      setShow(false)
    }).catch(() => {
      toast.error("Error al actualizar los datos.")
    });

    setLoading(false)
    window.location.reload()
  }

  return (
    <div className="form-edit">
      <div 
        className="banner" 
        style={{ backgroundImage: `url('${bannerUrl}')` }}
        {...getRootBannerProps()}>
          <input {...getInputBannerProps()} />
          <Camera />

      </div>
      <div 
        className="avatar" 
        style={{ backgroundImage: `url('${avatarUrl}')` }}
        {...getRootAvatarProps()}>
          <input {...getInputAvatarProps()} />
          <Camera />

      </div>
      <Form onSubmit={onSubmit}>
        <Form.Group>
          <Row>
            <Col>
              <Form.Control type="text" placeholder="Nombre" name="nombre" onChange={onChange} defaultValue={formData.nombre}>

              </Form.Control>
            </Col>
            <Col>
              <Form.Control type="text" placeholder="Apellidos" name="apellidos" onChange={onChange} defaultValue={formData.apellidos}>

              </Form.Control>
            </Col>
          </Row>
        </Form.Group>
        <Form.Group>
          <Form.Control rows="3" as="textarea" placeholder="Biografía..." type="text" onChange={onChange} name="biografia" defaultValue={formData.biografia}>
          </Form.Control>
        </Form.Group>

        <Form.Group>
          <Form.Control type="text" placeholder="Sitio web..." name="sitio_web" onChange={onChange} defaultValue={formData.sitio_web}>
          </Form.Control>
        </Form.Group>

        <Form.Group>
          <DatePicker placeholder="Fecha de nacimiento"
          locale={es}
          selected={new Date(formData.fecha_nacimiento)} 
          onChange={value => setFormData({...formData, fecha_nacimiento: value})} />
        </Form.Group>

        <Button className="btn btn-submit" variant="primary" type="submit" disbaled={loading}>
          {loading && ( <Spinner animation="border" size="sm" /> ) } Actualizar
        </Button>
      </Form>
    </div>
  )
}

function initialFormValue(user) {
  return {
    nombre: user.nombre ? user.nombre : "",
    apellidos: user.apellidos || "", //Esta forma de \\ hace lo mismo que lo de arriba, es decir, si esto tiene data, colocalo igual, sino vacío.
    biografia: user.biografia || "",
    ubicacion: user.ubicacion || "",
    sitio_web: user.sitio_web || "",
    fecha_nacimiento: user.fecha_nacimiento || ""
  }
}